<?php
define('S_POST', 'sanitized_post');
define('OPCIONS_VALIDAS', array('a', 'b', 'c'));
define('DEBUG', TRUE);

$data = array();
$data['titulo'] = "Iterativas 01";
$data["div_titulo"] = "Ejercicios arrays";

//Por comodidad creamos arrays para las variables que se pueden recibir por post y son arrays


//Comprobamos si se ha enviado el formulario y si es así, lo procesamos
if(isset($_POST['submit'])){
    $data['formSent'] = TRUE;
    /*
     * A efectos de depuración
     */
    $data['post'] = $_POST;
    /*
     * Primero comprobamos los errores y después si no hay errores, se procesaría el formulario. Nombre es un campo obligatorio pero textarea no por eso no lo comprobamos.
     */
    $data['errors'] = checkForm($_POST);
    //echo json_encode($data['errors']);die;
    
    /*
     * Dependiendo del escenario, nos interesa mostrar los datos que insertó el usuario dentro del formulario. Por ejemplo, en un formulario de inserción de datos,
     * si hay un error, no podemos obligar al usuario a meter todos los datos de nuevo si no que debemos mostrar lo que insertó y los errores que hubo. Las variables que insertó
     * debemos "limpiarlas" antes de mostrarlas (por ejemplo quitar etiquetas HTML) ya que un usuario con cierto conocimiento podría desmontarnos nuestra página insertando determinados
     * valores en el código.
     */
    $data[S_POST] = sanitizeInput($_POST);
    
    /**
     * Si no hay errores, ejecutamos la rutina predefinida. Por ejemplo guardar/modificar un registro de datos, generar un documento o realizar cálculos.
     */
    if(count($data['errors']) == 0){
        //Código que realizar la tarea para la que está creado el formulario.
        if($data[S_POST]['numEjercicio'] == 1){
            $_nums = explode(",", $data[S_POST]['arrayNumeros']);
            //var_dump($_nums);
            if(count($_nums) > 0){
                $min = $_nums[0];
                $max = $_nums[0];
                foreach($_nums as $n){
                    if($n < $min){
                        $min = $n;
                    }
                    elseif($n > $max){
                        $max = $n;
                    }
                }
            }
            $data['resultado'] = "El número mínimo es $min y el máximo es $max";
        }
        if($data[S_POST]['numEjercicio'] == 2){
            $_nums = explode(",", $data[S_POST]['arrayNumeros']);
            for($i = 0; $i < count($_nums) - 1; $i++){
                for($j = $i + 1; $j < count($_nums); $j++){
                    if($_nums[$i] > $_nums[$j]){
                        $aux = $_nums[$i];
                        $_nums[$i] = $_nums[$j];
                        $_nums[$j] = $aux;
                    }
                }
            }
            $data['resultado'] = "Resultado [".implode(", ", $_nums)."]";
        }
    }
    
    
}

function checkForm(array $_p) : array{
    $_errors = array();
    if(strlen($_p['arrayNumeros']) == 0){
        $_errors['arrayNumeros'] = "No ha insertado contenido";
    }
    elseif(strlen(filter_var($_p['arrayNumeros'], FILTER_SANITIZE_STRING)) == 0){
        $_errors['arrayNumeros'] = "El contenido no es válido porque contiene HTML";
    }
    else{
        $_aux = explode(",", $_p['arrayNumeros']);
        foreach($_aux as $num){
            if(!is_numeric($num)){
                if(!isset($_errors['arrayNumeros'])){
                    $_errors['arrayNumeros'] = "";
                }
                $_errors['arrayNumeros'] .= "'$num' no es un número. "; 
            }
        }
    }
    if(!filter_var($_p['numEjercicio'], FILTER_VALIDATE_INT) || (int)$_p['numEjercicio'] < 1 || (int)$_p['numEjercicio'] > 2){
        $_errors['numEjercicio'] = "Seleccione un valor válido";
    }
    return $_errors;
}

function sanitizeInput(array $_p): array{    
    $_data = array();
    $_data['arrayNumeros'] = filter_var($_p['arrayNumeros'], FILTER_SANITIZE_STRING);
    $_data['numEjercicio'] = filter_var($_p['numEjercicio'], FILTER_SANITIZE_STRING);
    return $_data;
}

include 'views/templates/header.php';
include 'views/iterativas01.view.php';
include 'views/templates/footer.php';