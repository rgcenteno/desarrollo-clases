<?php
define('S_POST', 'sanitized_post');
define('OPCIONS_VALIDAS', array('a', 'b', 'c'));
define('DEBUG', FALSE);

$data = array();
$data['titulo'] = "Iterativas 03";
$data["div_titulo"] = "Ejercicio matriz";

//Por comodidad creamos arrays para las variables que se pueden recibir por post y son arrays


//Comprobamos si se ha enviado el formulario y si es así, lo procesamos
if(isset($_POST['submit'])){
    $data['formSent'] = TRUE;
    /*
     * A efectos de depuración
     */
    $data['post'] = $_POST;
    /*
     * Primero comprobamos los errores y después si no hay errores, se procesaría el formulario. Nombre es un campo obligatorio pero textarea no por eso no lo comprobamos.
     */
    $data['errors'] = checkForm($_POST);
    //echo json_encode($data['errors']);die;
    
    /*
     * Dependiendo del escenario, nos interesa mostrar los datos que insertó el usuario dentro del formulario. Por ejemplo, en un formulario de inserción de datos,
     * si hay un error, no podemos obligar al usuario a meter todos los datos de nuevo si no que debemos mostrar lo que insertó y los errores que hubo. Las variables que insertó
     * debemos "limpiarlas" antes de mostrarlas (por ejemplo quitar etiquetas HTML) ya que un usuario con cierto conocimiento podría desmontarnos nuestra página insertando determinados
     * valores en el código.
     */
    $data[S_POST] = sanitizeInput($_POST);
    
    /**
     * Si no hay errores, ejecutamos la rutina predefinida. Por ejemplo guardar/modificar un registro de datos, generar un documento o realizar cálculos.
     */
    if(count($data['errors']) == 0){
        $_matriz = stringToMatrix(filter_var($_POST['arrayNumeros'], FILTER_SANITIZE_STRING));
        for($filaActual = 0; $filaActual < count($_matriz); $filaActual++){
            for($columnaActual = 0; $columnaActual < count($_matriz[$filaActual]); $columnaActual++){
                if($columnaActual == (count($_matriz[$filaActual]) - 1)){
                    $filaBusqueda = $filaActual + 1;
                    $salto = true;
                }
                else{
                    $filaBusqueda = $filaActual;
                    $salto = false;
                }
                //$filaBusqueda = ($columnaActual == (count($_matriz[$filaActual]) - 1)) ? $filaActual + 1 : $filaActual;
                for(;$filaBusqueda < count($_matriz); $filaBusqueda++){
                    if($filaActual != $filaBusqueda){
                        $columnaBusqueda = 0;
                    }
                    else{
                        $columnaBusqueda = $columnaActual + 1;
                    }
                    //$columnaBusqueda = ($filaActual != $filaBusqueda) ? 0 : $columnaActual + 1;
                    for(; $columnaBusqueda < count($_matriz[$filaBusqueda]); $columnaBusqueda++){
                        //echo "[$filaActual][$columnaActual] = ".$_matriz[$filaActual][$columnaActual]." > "."[$filaBusqueda][$columnaBusqueda] = ".$_matriz[$filaBusqueda][$columnaBusqueda]."<br />";
                        if($_matriz[$filaActual][$columnaActual] > $_matriz[$filaBusqueda][$columnaBusqueda]){
                            //echo "true";
                            $aux = $_matriz[$filaActual][$columnaActual];
                            $_matriz[$filaActual][$columnaActual] = $_matriz[$filaBusqueda][$columnaBusqueda];
                            $_matriz[$filaBusqueda][$columnaBusqueda] = $aux;
                        }
                    }
                }
            }
        } 
        $data['resultado'] = "";
        foreach($_matriz as $fila){
            $data['resultado'] .= "<p>".implode(",", $fila)."</p>";
        }
        
        /*
         * $data['resultado'] = "<table class='table'><tbody>";
            foreach($_matriz as $fila){
            $data['resultado'] .= "<tr><td>";
            $data['resultado'] .= implode("</td><td>", $fila);
            $data['resultado'] .= "</td></tr>";
        }
        $data['resultado'] .= "</table></table>";
         */
    }
    
    
}

function checkForm(array $_p) : array{
    $_errors = array();
    if(strlen($_p['arrayNumeros']) == 0){
        $_errors['arrayNumeros'] = "No ha insertado contenido";
    }
    elseif(strlen(filter_var($_p['arrayNumeros'], FILTER_SANITIZE_STRING)) == 0){
        $_errors['arrayNumeros'] = "El contenido no es válido porque contiene HTML";
    }
    else{
        $_arrayNums = stringToMatrix($_p['arrayNumeros']);
        
        foreach($_arrayNums as $_fila){
            foreach($_fila as $num){
                if(!is_numeric($num)){
                    if(!isset($_errors['arrayNumeros'])){
                        $_errors['arrayNumeros'] = "";
                    }
                    $_errors['arrayNumeros'] .= "'$num' no es un número. "; 
                }
            }
        }
    }    
    return $_errors;
}

function sanitizeInput(array $_p): array{    
    $_data = array();
    $_data['arrayNumeros'] = filter_var($_p['arrayNumeros'], FILTER_SANITIZE_STRING);    
    return $_data;
}

function stringToMatrix(string $text) : array{
    $_arrayStrings = explode("|", $text);
    $_arrayNums = array();
    foreach($_arrayStrings as $strings){
        $_arrayNums[] = explode(",", $strings);
    }
    return $_arrayNums;
}

include 'views/templates/header.php';
include 'views/iterativas03.view.php';
include 'views/templates/footer.php';