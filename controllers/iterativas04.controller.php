<?php
define('S_POST', 'sanitized_post');
define('DEBUG', FALSE);

$data = array();
$data['titulo'] = "Iterativas 04";
$data["div_titulo"] = "Contar letras";

//Por comodidad creamos arrays para las variables que se pueden recibir por post y son arrays


//Comprobamos si se ha enviado el formulario y si es así, lo procesamos
if(isset($_POST['submit'])){
    $data['formSent'] = TRUE;
    /*
     * A efectos de depuración
     */
    $data['post'] = $_POST;
    /*
     * Primero comprobamos los errores y después si no hay errores, se procesaría el formulario. Nombre es un campo obligatorio pero textarea no por eso no lo comprobamos.
     */
//    $data['errors'] = checkForm($_POST);
    //echo json_encode($data['errors']);die;
    
    /*
     * Dependiendo del escenario, nos interesa mostrar los datos que insertó el usuario dentro del formulario. Por ejemplo, en un formulario de inserción de datos,
     * si hay un error, no podemos obligar al usuario a meter todos los datos de nuevo si no que debemos mostrar lo que insertó y los errores que hubo. Las variables que insertó
     * debemos "limpiarlas" antes de mostrarlas (por ejemplo quitar etiquetas HTML) ya que un usuario con cierto conocimiento podría desmontarnos nuestra página insertando determinados
     * valores en el código.
     */
    $data[S_POST] = sanitizeInput($_POST);
    
    /**
     * Si no hay errores, ejecutamos la rutina predefinida. Por ejemplo guardar/modificar un registro de datos, generar un documento o realizar cálculos.
     */
    $texto = strtolower(preg_replace("/[^a-zA-Z0-9]+/", " ", $_POST['texto']));
    echo $texto;
    $_arrayChar = str_split($texto);
    $_resultado = array();
    foreach($_arrayChar as $letra){
        if(isset($_resultado[$letra])){
            $_resultado[$letra]++;
        }
        else{
            $_resultado[$letra] = 1;
        }
        
    }
    arsort($_resultado);
    $data["resultado"] = "";
    foreach($_resultado as $letra => $contador){
        $data["resultado"] .= "<p>$letra: $contador</p>";
    }
}

function checkForm(array $_p) : array{
    $_errors = array();
    if(strlen($_p['texto']) == 0){
        $_errors['texto'] = "No ha insertado contenido";
    }     
    return $_errors;
}

function sanitizeInput(array $_p): array{    
    $_data = array();
    $_data['texto'] = filter_var($_p['texto'], FILTER_SANITIZE_STRING);    
    return $_data;
}

include 'views/templates/header.php';
include 'views/iterativas04.view.php';
include 'views/templates/footer.php';