<?php
define('S_POST', 'sanitized_post');
define('DEBUG', TRUE);

$data = array();
$data['titulo'] = "Iterativas 05";
$data["div_titulo"] = "Contar palabras";

//Por comodidad creamos arrays para las variables que se pueden recibir por post y son arrays


//Comprobamos si se ha enviado el formulario y si es así, lo procesamos
if(isset($_POST['submit'])){
    $data['formSent'] = TRUE;
    /*
     * A efectos de depuración
     */
    $data['post'] = $_POST;
    
    $data['errors'] = checkForm($_POST);
    /*
     * Primero comprobamos los errores y después si no hay errores, se procesaría el formulario. Nombre es un campo obligatorio pero textarea no por eso no lo comprobamos.
     */
//    $data['errors'] = checkForm($_POST);
    //echo json_encode($data['errors']);die;
    
    /*
     * Dependiendo del escenario, nos interesa mostrar los datos que insertó el usuario dentro del formulario. Por ejemplo, en un formulario de inserción de datos,
     * si hay un error, no podemos obligar al usuario a meter todos los datos de nuevo si no que debemos mostrar lo que insertó y los errores que hubo. Las variables que insertó
     * debemos "limpiarlas" antes de mostrarlas (por ejemplo quitar etiquetas HTML) ya que un usuario con cierto conocimiento podría desmontarnos nuestra página insertando determinados
     * valores en el código.
     */
    $data[S_POST] = sanitizeInput($_POST);
    
    /**
     * Si no hay errores, ejecutamos la rutina predefinida. Por ejemplo guardar/modificar un registro de datos, generar un documento o realizar cálculos.
     */
    if(count($data['errors']) == 0){
        $texto = strtolower(preg_replace("/[^a-zA-Z0-9 ]+/", " ", $_POST['texto']));//Ponemos un espacio para que cuenten como palabras diferentes
        $texto = trim(preg_replace('/\s{2,}/', " ", $texto));
        //echo "'$texto'";
        $_arrayWord = explode(" ", $texto);
        //var_dump($_arrayWord);
        $_resultado = array();
        foreach($_arrayWord as $palabra){
            if(isset($_resultado[$palabra])){
                $_resultado[$palabra]++;
            }
            else{
                $_resultado[$palabra] = 1;
            }

        }
        arsort($_resultado);
        $data["resultado"] = "";
        foreach($_resultado as $palabra => $contador){
            $data["resultado"] .= "<p>'$palabra': $contador</p>";
        }
    }
}

function checkForm(array $_p) : array{
    $_errors = array();
    if(strlen($_p['texto']) == 0){
        $_errors['texto'] = "No ha insertado contenido";
    }     
    return $_errors;
}

function sanitizeInput(array $_p): array{    
    $_data = array();
    $_data['texto'] = htmlentities($_p['texto']);    
    return $_data;
}

include 'views/templates/header.php';
include 'views/iterativas05.view.php';
include 'views/templates/footer.php';