<?php
define('S_POST', 'spost');
define('DEBUG', false);

$data = array();

$data['titulo'] = "Ejercicio 06";
$data['div_titulo'] = "Criba de erastótenes";

if(isset($_POST['submit'])){
    $data['errors'] = checkErrors($_POST);
    $data[S_POST] = sanitizeInput($_POST);
    if(count($data['errors']) == 0){
        $data['resultado'] = implode(", ", cribaErastótenes($_POST['limite']));
    }
}

function checkErrors(array $_p) : array{
    $_errors = array();
    if(!filter_var($_p['limite'], FILTER_VALIDATE_INT)){
        $_errors['limite'] = 'Inserte un número entero';
    }
    elseif($_p['limite'] < 2){
        $_errors['limite'] = 'Inserte un número mayor o igual que 2.';
    }
    return $_errors;
}

function sanitizeInput($_p){
    $_res = array();
    $_res['limite'] = filter_var($_p['limite'], FILTER_VALIDATE_INT) ? $_p['limite'] : "";
    return $_res;
}

function cribaErastótenes(int $limite) : array{
    $_numeros = array();
    for($i = 2; $i <= $limite; $i++){
        array_push($_numeros, $i);
        //$_numeros[] = $i;
    }
    
    for($i = 2; $i*$i <= $limite; $i++){
        if(array_search($i, $_numeros) !== FALSE){
            for($j = $i; $i * $j <= $limite; $j++){
                $posicion = array_search($i*$j, $_numeros);
                if($posicion !== FALSE){
                    //echo $posicion;
                    unset($_numeros[$posicion]);
                    //var_dump($_numeros);
                }
            }
        }
    }
    return $_numeros;
}

include 'views/templates/header.php';
include 'views/iterativas06.view.php';
include 'views/templates/footer.php';