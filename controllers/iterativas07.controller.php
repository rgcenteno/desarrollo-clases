<?php
define('S_POST', 'spost');
define('DEBUG', false);
define('LIMITE', 90);
define('TAM_CARTON', 15);

$data = array();

$data['titulo'] = "Ejercicio 07";
$data['div_titulo'] = "Bingo";

if(isset($_GET['sacar']) ){
    if(isset($_GET['bolas'])){
        $_bolas = json_decode(urldecode($_GET['bolas']));
    }
    else{
        $_bolas = array();
    }
    if(isset($_GET['carton'])){
        $_carton = json_decode(urldecode($_GET['carton']));
    }
    else{
        $_carton = generarCarton();
    } 
    if(count(array_diff($_carton, $_bolas)) > 0 ){
        $_bolas[] = sacarBola($_bolas);
    }
}
else{
    $_bolas = array();
    $_carton = generarCarton();
}

function sacarBola(array $_bolas) : int{
    do{
        $bola = rand(1, 90);
    }
    while(in_array($bola, $_bolas));
    return $bola;
}

function generarCarton() : array{
    $_carton = array();
    for($i = 0; $i < TAM_CARTON; $i++){
        do{
            $num = rand(1, LIMITE);
        }while(in_array($num, $_carton));
        $_carton[] = $num;
    }
    sort($_carton);
    return $_carton;
}

function generarCartonMarcado(array $_carton, array $_bolas) : array{
    $_aux = array();
    foreach($_carton as $valor){
        if(in_array($valor, $_bolas)){
            $_aux[] = '<label class="text-danger"><strong>'.$valor.'</strong></label>';
        }
        else{
            $_aux[] = $valor;
        }
    }
    return $_aux;
}

$data['bolas'] = $_bolas;
$data['carton'] = $_carton;
$data['cartonString'] = generarCartonMarcado($_carton, $_bolas);
$data['win'] = count(array_diff($_carton, $_bolas)) == 0;
//var_dump($_carton);
//var_dump($_bolas);

include 'views/templates/header.php';
include 'views/iterativas07.view.php';
include 'views/templates/footer.php';