<?php

/*include 'helpers/Persona.php';
include 'helpers/Categoria.php';
include 'helpers/Producto.php';
include 'helpers/Proveedor.php';
include 'helpers/ArgumentoNoValidoException.php';*/

use Com\Daw2\Categoria;
use Com\Daw2\Persona;
use Com\Daw2\Proveedor;
use Com\Daw2\Producto;


$cat = new Com\Daw2\Categoria(null, "Electronica");

$p = new Persona("Rafa", "González Centeno");
echo $p->nombre." ".$p->apellidos;

$c1 = new Categoria(null, "Electrónica");
$c2 = new Categoria($c1, "Móviles");
$c3 = new Categoria($c2, "Xiaomi");

echo "<p>".htmlspecialchars($c1->getFullName())."</p>";
echo "<p>".htmlspecialchars($c2->getFullName())."</p>";
echo "<p>".htmlspecialchars($c3->getFullName())."</p>";
echo $c3->nombre;
$c3->padre = null;
$c3->nombre = "prueba";
echo "<p>".htmlspecialchars($c3->getFullName())."</p>";

//(string $cif, string $codigo, string $nombre, string $direccion, string $website, string $pais, string $email)
$pr1 = new Proveedor("D16742579", "Pr001", "Proveedor 1", "Dirección prueba", "https://website.com", "Spain", "test@email.org");
$pr1->direccion = "Test direccion";
//$pr1->cif = "abc"; //Debe dar excepción
var_dump($pr1);
//(string $codigo, string $nombre, string $descripcion, Proveedor $p, Categoria $cat, int $stock, float $coste, float $margen, float $iva) {
$producto = new Producto("Codigo", "nombre", "descripcion", $pr1, $c3, 100, 50, 1.1, 21);
echo "<p>Producto precio venta con IVA: ".$producto->getPrecioVenta(true)."</p>";
//$producto->stock = 30;
echo $producto->stock;
$test = new Com\Daw2\Test\TestClass();
