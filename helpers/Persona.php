<?php

namespace Com\Daw2;

class Persona{
    private $nombre;
    private $apellidos;
    
    function __construct(string $n, string $a){
        $this->apellidos = $a;
        $this->nombre = $n;
    }
    
    
    public function __get($atributo) {
        if($atributo == "apellidos"){
            return "Sin permiso";
        }
        return $this->$atributo;
    }
}

