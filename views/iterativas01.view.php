<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><?php echo $data['titulo']; ?></h1>

</div>

<!-- Content Row -->

<div class="row">
    <?php
    if (DEBUG && isset($data['post'])) {
        ?>
        <div class="col-12">
            <div class="card shadow mb-4">
                <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Variables pasadas:</h6>                                    
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <?php var_dump($data['post']); ?>
                    <?php var_dump($data['sanitized_post']); ?>
                    <?php var_dump($data['errors']); ?>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
    <div class="col-12">
        <div class="card shadow mb-4">
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary"><?php echo $data['div_titulo']; ?></h6>                                    
            </div>
            <!-- Card Body -->
            <div class="card-body">

                <form action="./?sec=iterativas01" method="post">
                    <div class="container-fluid">
                        <div class="row">
                            <?php
                            if(isset($data['resultado'])){
                            ?>                            
                            <div class="alert alert-success col-12">
                                <?php echo $data['resultado']; ?>
                            </div>
                            <?php
                            }
                            ?>
                            <!--<form method="get">-->                            
                            <div class="mb-3 col-lg-6">
                                <label for="arrayNumeros">Array de números<span class="text-danger">*</span></label>
                                <input class="form-control" id="arrayNumeros" type="text" name="arrayNumeros" placeholder="Inserte números separados por comas" value="<?php echo isset($data[S_POST]['arrayNumeros']) ? $data[S_POST]['arrayNumeros'] : ""; ?>">
                                <?php if(isset($data['errors']) && isset($data['errors']['arrayNumeros'])){ ?>
                                <p class="text-danger"><small><?php echo $data['errors']['arrayNumeros']; ?></small></p>
                                <?php } ?>
                            </div>                            
                            <div class="mb-3 col-lg-6">
                                <label for="numEjercicio">Selector ejercicio</label>
                                <select class="form-control" id="numEjercicio" name="numEjercicio">
                                    <option value="0">--Seleccione ejercicio--</option>
                                    <option value="1" <?php echo isset($data[S_POST]['numEjercicio']) && $data[S_POST]['numEjercicio'] === "1" ? 'selected' : ""; ?>>Ejercicio 1</option>
                                    <option value="2" <?php echo isset($data[S_POST]['numEjercicio']) && $data[S_POST]['numEjercicio'] === "2" ? 'selected' : ""; ?>>Ejercicio 2</option>                                    
                                </select>
                                <?php if(isset($data['errors']) && isset($data['errors']['numEjercicio'])){ ?>
                                <p class="text-danger"><small><?php echo $data['errors']['numEjercicio']; ?></small></p>
                                <?php } ?>
                            </div>                            
                            <div class="mb-3 col-lg-12">
                                <input type="submit" value="Enviar" name="submit" class="btn btn-primary"/>
                            </div>
                        </div>
                    </div>
                </form>
                
            </div>
        </div>
    </div>                        
</div>

