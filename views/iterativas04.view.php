<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><?php echo $data['titulo']; ?></h1>

</div>

<!-- Content Row -->

<div class="row">
    <?php
    if (DEBUG && isset($data['post'])) {
        ?>
        <div class="col-12">
            <div class="card shadow mb-4">
                <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Variables pasadas:</h6>                                    
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <?php var_dump($data['post']); ?>
                    <?php var_dump($data['sanitized_post']); ?>
                    <?php var_dump($data['errors']); ?>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
    <div class="col-12">
        <div class="card shadow mb-4">
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary"><?php echo $data['div_titulo']; ?></h6>                                    
            </div>
            <!-- Card Body -->
            <div class="card-body">

                <form action="./?sec=iterativas04" method="post">
                    <div class="container-fluid">
                        <div class="row">
                            <?php
                            if(isset($data['resultado'])){
                            ?>                            
                            <div class="alert alert-success col-12">
                                <?php echo $data['resultado']; ?>
                            </div>
                            <?php
                            }
                            ?>
                            <!--<form method="get">-->                            
                            <div class="mb-3 col-lg-12">
                                <label for="texto">Texto<span class="text-danger">*</span></label>
                                <input class="form-control" id="texto" type="text" name="texto" placeholder="Inserte el texto a analizar. Sólo letras y números serán analizadas." value="<?php echo isset($data[S_POST]['texto']) ? $data[S_POST]['texto'] : ""; ?>">                                
                                <?php if(isset($data['errors']) && isset($data['errors']['texto'])){ ?>
                                <p class="text-danger"><small><?php echo $data['errors']['texto']; ?></small></p>
                                <?php } ?>
                            </div>                                                                                 
                            <div class="mb-3 col-lg-12">
                                <input type="submit" value="Enviar" name="submit" class="btn btn-primary"/>
                            </div>
                        </div>
                    </div>
                </form>                
            </div>
        </div>
    </div>                        
</div>

