<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><?php echo $data['titulo']; ?></h1>

</div>

<!-- Content Row -->

<div class="row">
    <?php
    if (DEBUG && isset($data['post'])) {
        ?>
        <div class="col-12">
            <div class="card shadow mb-4">
                <div
                    class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Variables pasadas:</h6>                                    
                </div>
                <!-- Card Body -->
                <div class="card-body">
                    <?php var_dump($data['post']); ?>
                    <?php var_dump($data['sanitized_post']); ?>
                    <?php var_dump($data['errors']); ?>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
    <div class="col-12">
        <div class="card shadow mb-4">
            <div
                class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary"><?php echo $data['div_titulo']; ?></h6>                                    
            </div>
            <!-- Card Body -->
            <div class="card-body">

                <form action="./?sec=iterativas07" method="get">
                    <div class="container-fluid">
                        <div class="row">
                            <?php
                            if($data['win']){
                                ?>
                            <div class="alert alert-success col-12 text-center">
                                ¡¡BINGO!!                              
                            </div>
                                <?php
                            }
                            ?>
                            <?php
                            if(isset($data['bolas']) && count($data['bolas']) > 0){
                            ?>                            
                            <div class="alert alert-primary col-xs-12 col-6">
                                <?php echo implode(", ", $data['bolas']); ?>                                
                            </div>
                            <div class="col-xs-12 col-6">
                                <table class="table table-bordered">
                                        <?php 
                                        for($i = 0; $i < count($data['cartonString']); $i++){
                                            if($i % 5 == 0){
                                                echo '<tr>';
                                            }
                                            echo '<td class="text-center">'.$data['cartonString'][$i].'</td>';
                                            if($i % 5 == 4){
                                                echo '</tr>';
                                            }
                                        }
                                        ?>
                                </table>
                                </div>
                            <?php
                            }
                            ?>
                            <input type="hidden" name="sec" value="iterativas07" />   
                            <input type="hidden" name="bolas" value="<?php echo urlencode(json_encode($data['bolas'])); ?>" />
                            <input type="hidden" name="carton" value="<?php echo urlencode(json_encode($data['carton'])); ?>" />
                            <div class="mb-3 col-md-12 text-center">
                                <input type="submit" value="Reiniciar Bingo" name="reiniciar" class="btn btn-danger"/> 
                                <input type="submit" value="Sacar bola &gt;&gt;" name="sacar" class="btn btn-primary  ml-3" />
                            </div>
                        </div>
                    </div>
                </form>                
            </div>
        </div>
    </div>                        
</div>

